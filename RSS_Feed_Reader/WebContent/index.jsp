<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
  
 <html>
  <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <title>jQuery + jGFeed AJAX Feed Reader Example</title>
    <!-- <link rel="stylesheet" href="assets/styles.css"
type="text/css" /> --> 
</head> 
<body> 
   <h1>jQuery + jGFeed AJAX Feed Reader Example</h1> 
  <ul class="menu"> 
 <li> <a class="ajax-feed-trigger" href="http://www.lupomontero.com/feed"> .lupomontero </a> </li>
  <li> <a class="ajax-feed-trigger"
href="http://feeds.feedburner.com/nettuts"> Nettuts+ </a> </li>
 <li> <a class="ajax-feed-trigger" href="http://feeds.feedburner.com/jquery"> jQuery </a> </li>
  <li> <a class="ajax-feed-trigger"
href="http://feeds2.feedburner.com/LearningJquery"> Learning jQuery </a> </li>
 </ul> 
  
 <div id="ajax-feed-wrapper"> <div id="ajax-feed-container"> Click links above to fetch feeds </div> </div> 
  
 <!-- Load jQuery, jGFeed and feedreader scripts --> 
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js">
 </script> <script type="text/javascript" src="assets/jquery.jgfeed-min.js">
 </script> <script type="text/javascript" src="assets/feedreader.js"></script> 
  
 </body> </html>