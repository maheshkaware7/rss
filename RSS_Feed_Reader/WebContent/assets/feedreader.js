$(document).ready(function() {
	$('a.ajax-feed-trigger').click(function(e) {
		e.preventDefault(); 
		var container=$('#ajax-feed-container');
		container.empty().addClass('loading');
		var href = $(this).attr('href');
		alert(href);
		
		// get feed using JGfeed
		$.jGFeed(href,function(feeds)
				{
			// check if error
			if(!feeds)
				{
				container.append('Error in fetching feed');
				return false;
				}
			container.hide();
			container.removeClass('loading');
			container.append('<h2>' + feeds.title + '</h2>');
			 // Process feed entries
			for (var i=0; i<feeds.entries.length; i++)
			{
				var entry = feeds.entries[i]; 
				  
				 // Build HTML string for entry
				var html = '<div class="ajax-feed-item">';
				html += '<hr /><h2><a href="' + entry.link + '">';
				html += entry.title + '</a></h2>';
				html += '<div class="ajax-feed-date">';
				html += entry.publishedDate + '</div>';
				html += '<div class="ajax-feed-author"> Posted by ';
				html += entry.author + '</div>'; 
				html += '<div class="ajax-feed-content-snippet">';
				html += entry.contentSnippet + '</div>';
				html += '<div id="ajax-feed-content-'+i;
				html += '" class="ajax-feed-content" ';
				html += 'style="height:0px; overflow:hidden;">';
				html += entry.content + '</div>';
				html += '<div><a class="ajax-feed-readmore" ';
				html += 'href="' + i + '">+</a></div>';
				html += '</div>'; 
				container.append(html);
				
			}
			 container.show('slow'); 
				}
		,5);// End of jGFeed
	}); // End a.ajax-feed-trigger click event
	
	});
